from suds.client import Client
from lxml import etree
import math

class ClientObject:
    
    def __init__(self, key_user="test", user_name="trumega_6@tedata.net.eg"):
        url = "http://10.252.4.52:7004/TEMWFEVLService?wsdl"
        self.client = Client(url, retxml=True)
        self.key_user = key_user
        self.user_name = user_name
										 
#Query Profile
    def query_profile(self, key_user, user_name):
        print etree.tostring(etree.fromstring(self.client.service.getUserQuota(key_user, user_name)), pretty_print=True)
        return

#Tal2aProvision
    def provision_tal2a(self, key_user, user_name, service_name, start_date, end_date, reset_consumed_quota, carry_over):
        print etree.tostring(etree.fromstring(self.client.service.provisionTal2aSubscriber(key_user, user_name, service_name, start_date, end_dateate, reset_consumed_quota, carry_overOver)), pretty_print=True)
        return

#Debit from Subscriber
    def debit_sub(self, key_user, user_name, debit):
        print etree.tostring(etree.fromstring(self.client.service.debitSubscriber(key_user, user_name, debit)), pretty_print=True)
        return
#Add Topup
    def add_topup_quota(self, key_user, user_name, credit_amount, bill_id):
        print etree.tostring(etree.fromstring(self.client.service.addTOPUPQuota(key_user, user_nameame, credit_amount, bill_id)), pretty_print=True)
        return



#scenario 1
print "Scenario 1"


print " Query Profile"
client1 = ClientObject()
client1.query_profile(client1.key_user, client1.user_name)

print " Provision Tal2a"
service_name =  "MONTHLY_1024_CAP"                                     
start_date = "2017-03-05 00:00:00"                                                   #Year-Month-Day Hour:Minute:Second
end_date =   "2017-04-05 23:59:59"
reset_consumed_quota =  True                                      #Boolean 
carry_over = 0                                                  #Float
client1.provision_tal2a(client1.keyUser, client1.user_name, service_name, start_date, end_date, reset_consumed_quota, carry_over)

print " Query Profile"
client1.query_profile(client1.key_user, client1.user_name)

print "Debit from User"
debit =  1024000000.0                                                     #Float
client1.debit_sub(client1.key_user, client1.user_name, debit)

print " Query Profile"
client1.query_profile(client1.key_user, client1.user_name)

print "Provision Tal2a with resetConsumedQuota False"
reset_consumed_quota =  False
client1.provisionTal2a(client1.keyUser, client1.user_name, service_name, start_date, end_date, reset_consumed_quota, carry_over)

print " Query Profile"
client1.query_profile(client1.key_user, client1.user_name)

print "Provision Tal2a with resetConsumedQuota True"
reset_consumed_quota =  True
client1.provision_tal2a(client1.key_user,client1.user_name, service_name, start_date, end_date, reset_consumed_quota, carry_over)

print " Query Profile"
client1.query_profile(client1.key_user, client1.user_name)



# scenario 2&3
print "Scenario 2&3"
client2 = ClientObject()


print " Query Profile"
client2.query_profile(client2.key_user, client2.user_name)

print " Provision Tal2a"
serviceName =  "MONTHLY_1024_CAP"                                     
start_date = "2017-03-05 00:00:00"                                                   #Year-Month-Day Hour:Minute:Second
endDate =   "2017-04-05 23:59:59"
reset_consumed_quota =  True                                      #Boolean 
carry_over = 0                                                  #Float
client2.provision_tal2a(client2.key_user, client2.user_name, service_name, start_date, end_date, reset_consumed_quota, carry_over)

print " Query Profile"
client2.query_profile(client2.key_user, client2.user_name)


print " Add Topup "
credit_amount =  1024000000.0                                                       #float
bill_id = "0"                                                                      #string
client2.add_topup_quota(client2.key_user, client2.user_name, credit_amount, bill_id)

print " Query Profile"
client2.query_profile(client2.key_user, client2.user_name)

print "Debit from User"
debit =  512000000.0                                                       #Float
client2.debit_sub(client2.key_user, client2.user_name, debit)

print " Query Profile"
client2.query_profile(client2.key_user, client2.user_name)

print "Provision Tal2a with resetConsumedQuota False and carryOver 40G"
reset_consumed_quota =  False
carry_over = 40000000000.0
client2.provision_tal2a(client2.key_user, client2.user_name, service_name, start_date, end_date, reset_consumed_quota, carry_over)

print " Query Profile"
client2.query_profile(client2.key_user, client2.user_name)
#### End of 2 & continue to 3

print "Provision Tal2a with carryOver zero"
carry_over = 0
client2.provision_tal2a(client2.key_user, client2.user_name, service_name, start_date, end_date, reset_consumed_quota, carry_over)
print " Query Profile"
client2.query_profile(client2.key_user, client2.user_name)

## scenario 4

print "Scenario 4"
client4 = ClientObject()

print " Query Profile"
client4.query_profile(client4.key_user, client4.user_name)

print "Provision Tal2a with wrong Date (logically)"
start_date = "2015-03-15 07:00:00"
end_date = "2015-03-15 07:00:00"
client4.provision_tal2a(client4.key_user, client4.user_name, service_name, start_date, end_date, reset_consumed_quota, carry_over)
