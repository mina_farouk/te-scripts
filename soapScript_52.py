
""" This is ascript for testing New MW(FrontEnd Only) by comparing the result(output is just XML and it has to be the same) with Current MW after applying this script on both by changing URL"""

from suds.client import Client
from lxml import etree
import math

class clientObject:
	def __init__(self, keyUser="keyuser", userName="username@tedata.net.eg"):
		url = "Path to WSDL file on server"                     
                self.client = Client(url, retxml=True)
                self.keyUser = keyUser
                self.userName = userName
										 
#Query Profile
	def queryProfile(self, keyUser, userName):		 
		 print etree.tostring(etree.fromstring(self.client.service.getUserQuota(keyUser, userName)), pretty_print=True)
		 return

#Tal2aProvision
	def provisionTal2a(self, keyUser, userName, serviceName, startDate, endDate, resetConsumedQuota, carryOver):
                print etree.tostring(etree.fromstring(self.client.service.provisionTal2aSubscriber(keyUser, userName, serviceName, startDate, endDate, resetConsumedQuota, carryOver)), pretty_print=True)
                return

#Debit from Subscriber
	def debitSub(self, keyUser, userName, debit):
                print etree.tostring(etree.fromstring(self.client.service.debitSubscriber(keyUser, userName, debit)), pretty_print=True)
	        return
#Add Topup
	def addTopUPQuota(self, keyUser, userName, creditAmount, billID):
                print etree.tostring(etree.fromstring(self.client.service.addTOPUPQuota(keyUser, userName, creditAmount, billID)), pretty_print=True)
		return

#client.functionName("Arguments")

#scenario 1
print "Scenario 1"


print " Query Profile"
#key = "xuser"                                                                                     #raw_input("Please Enter User Key: ")
#userName = "trumega_500@tedata.net.eg"
client1 = clientObject()                                                                                #raw_input("Please Enter User Name: ")
client1.queryProfile(client1.keyUser, client1.userName)

print " Provision Tal2a"
serviceName =  "MONTHLY_1024_CAP"                                     
startDate = "2017-03-05 00:00:00"                                                   #Year-Month-Day Hour:Minute:Second
endDate =   "2017-04-05 23:59:59"
resetConsumedQuota =  True                                      #Boolean 
carryOver = float("0")                                                  #Float   
client1.provisionTal2a(client1.keyUser, client1.userName, serviceName, startDate, endDate, resetConsumedQuota, carryOver)

print " Query Profile"
client1.queryProfile(client1.keyUser, client1.userName)

print "Debit from User"
debit =  float("1024000000")                                                       #Float
client1.debitSub(client1.keyUser, client1.userName, debit)

print " Query Profile"
client1.queryProfile(client1.keyUser, client1.userName)

print "Provision Tal2a with resetConsumedQuota False"
resetConsumedQuota =  False
client1.provisionTal2a(client1.keyUser, client1.userName, serviceName, startDate, endDate, resetConsumedQuota, carryOver)

print " Query Profile"
client1.queryProfile(client1.keyUser, client1.userName)

print "Provision Tal2a with resetConsumedQuota True"
resetConsumedQuota =  True
client1.provisionTal2a(client1.keyUser,client1.userName, serviceName, startDate, endDate, resetConsumedQuota, carryOver)

print " Query Profile"
client1.queryProfile(client1.keyUser, client1.userName)



# scenario 2&3
print "Scenario 2&3"
client2 = clientObject()


print " Query Profile"
#key = "xuser"                                         #raw_input("Please Enter User Key: ")
#userName = "trumega_500@tedata.net.eg"                                      #raw_input("Please Enter User Name: ")
client2.queryProfile(client2.keyUser, client2.userName)

print " Provision Tal2a"
serviceName =  "MONTHLY_1024_CAP"                                     
startDate = "2017-03-05 00:00:00"                                                   #Year-Month-Day Hour:Minute:Second
endDate =   "2017-04-05 23:59:59"
resetConsumedQuota =  True                                      #Boolean 
carryOver = float("0")                                                  #Float   
client2.provisionTal2a(client2.keyUser, client2.userName, serviceName, startDate, endDate, resetConsumedQuota, carryOver)

print " Query Profile"
client2.queryProfile(client2.keyUser, client2.userName)


print " Add Topup"
creditAmount =  float("1024000000")                                                       #float
billID = "0"                                                                      #string
client2.addTopUPQuota(client2.keyUser, client2.userName, creditAmount, billID)

print " Query Profile"
client2.queryProfile(client2.keyUser, client2.userName)

print "Debit from User"
debit =  float("512000000")                                                       #Float
client2.debitSub(client2.keyUser, client2.userName, debit)

print " Query Profile"
client2.queryProfile(client2.keyUser, client2.userName)

print "Provision Tal2a with resetConsumedQuota False and carryOver 40G"
resetConsumedQuota =  False
carryOver = float("40000000000")
client2.provisionTal2a(client2.keyUser, client2.userName, serviceName, startDate, endDate, resetConsumedQuota, carryOver)

print " Query Profile"
client2.queryProfile(client2.keyUser, client2.userName)
#### End of 2 & continue to 3

print "Provision Tal2a with carryOver zero"
carryOver = float("0")
client2.provisionTal2a(client2.keyUser, client2.userName, serviceName, startDate, endDate, resetConsumedQuota, carryOver)
print " Query Profile"
client2.queryProfile(client2.keyUser, client2.userName)

## scenario 4

print "Scenario 4"
client4 = clientObject()

print " Query Profile"
client4.queryProfile(client4.keyUser, client4.userName)

print "Provision Tal2a with wrong Date (logically)"
startDate = "2015-03-15 07:00:00"
endDate = "2015-03-15 07:00:00"
client4.provisionTal2a(client4.keyUser, client4.userName, serviceName, startDate, endDate, resetConsumedQuota, carryOver)





	
