import unittest
import test_48
import test_52
import xmlunittest
import string
class MyTest(unittest.TestCase, xmlunittest.XmlTestMixin):
    def test_query(self):
        client_48 = test_48.clientObject()
        client_52 = test_52.clientObject()
        print "Testing Query Profile"
        self.assertXmlEquivalentOutputs(client_48.queryProfile(client_48.keyUser, client_48.userName), client_52.queryProfile(client_52.keyUser, client_52.userName))

#  def test_provision(self):

#     self.assertEqual(test_48.query_48, test_52.provision_1024)


def main():
    unittest.main()

if __name__ == '__main__':
    main()
